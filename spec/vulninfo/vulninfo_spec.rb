# frozen_string_literal: true

RSpec.describe Vulninfo do
  it "provides accesss to ontology" do
    node = Ontology.nodes['cwe-78']

    desc = "The product constructs all or part of an OS command using externally-influenced input from an upstream component, but it does not neutralize or incorrectly neutralizes special elements that could modify the intended OS command when it is sent to a downstream component."
    title = "Improper Neutralization of Special Elements used in an OS Command ('OS Command Injection')"

    expect(node).to be_truthy

    puts node[:description]
    expect(node[:description]).to eq(desc)
    expect(node[:title]).to eq(title)
  end
end
