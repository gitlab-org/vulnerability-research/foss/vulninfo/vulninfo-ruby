# frozen_string_literal: true

RSpec.describe Slicer do
  it "correclty compute the common ancestors" do
    ancestors = Slicer.common_ancestors('cwe-39', 'cwe-32')
    expect(ancestors.first.first.node).to eq('cwe-22')
    expect(ancestors.first.first.level).to eq(2)
    expect(ancestors.first.second.node).to eq('cwe-22')
    expect(ancestors.first.second.level).to eq(2)
  end

  it "correclty slices ontology" do
    cwe22 = Ontology.nodes['cwe-22']
    slice, _ = Slicer.slice(cwe22[:identifier], Slicer::Direction::FORWARD)

    expect = [
    	"cwe-36:parent_of:cwe-38",
    	"cwe-23:parent_of:cwe-27",
    	"cwe-23:parent_of:cwe-28",
    	"cwe-23:parent_of:cwe-29",
    	"cwe-23:parent_of:cwe-33",
    	"cwe-22:parent_of:cwe-36",
    	"cwe-36:parent_of:cwe-39",
    	"cwe-23:parent_of:cwe-31",
    	"cwe-36:parent_of:cwe-37",
    	"cwe-36:parent_of:cwe-40",
    	"cwe-23:parent_of:cwe-24",
    	"cwe-23:parent_of:cwe-25",
    	"cwe-23:parent_of:cwe-26",
    	"cwe-23:parent_of:cwe-30",
    	"cwe-23:parent_of:cwe-34",
    	"cwe-23:parent_of:cwe-35",
    	"cwe-22:parent_of:cwe-23",
    	"cwe-23:parent_of:cwe-32",
    ]

    expect(slice.relations.size).to eq(expect.size)

    expect.each do |expectation|
    	src, rel, dest = expectation.split(":")
    	expect(slice.nodes[src][:identifier]).to eq(src)
    	expect(slice.nodes[dest][:identifier]).to eq(dest)
    	expect(slice.relations[expectation][:identifier]).to eq(expectation)
    	expect(slice.relations[expectation][:label]).to eq(rel)
    	expect(slice.out_relations[src].include?(src))
    	expect(slice.in_relations[dest].include?(dest))
    end
  end
end
